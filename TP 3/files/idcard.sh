#!/bin/bash

# Définition des variables
name=$(cat /etc/hostname)
os=$(/etc/os-release | grep -w "NAME" | cut -d'"' -f2)
kernel_version=$(uname -r)
ip=$(ip a | grep inet | grep enp0s8 | tr -s '[:space:]' | cut -d' ' -f3)
ram=$(free -h --giga | grep Mem: | tr -s '[:space:]')
storage=$(df -h / | grep / | tr -s '[:space:]' | cut -d' ' -f4)
top_processes=$(ps aux --no-headers --sort -rss | head -n5)
ports=$(ss -alnptH | tr -s '[:space:]')
random_cat=$(curl -s https://api.thecatapi.com/v1/images/search | cut -c 2- | rev | cut -c 2- | rev | jq -r ".url")


# Script & affichage des différents éléments
echo "Machine name : $name"
echo "OS : $os and kernel version is $kernel_version"
echo "IP : $ip"
echo "RAM : $(echo "$ram" | cut -d' ' -f4) / $(echo "$ram" | cut -d' ' -f2)"
echo "Disque : $storage space left"
echo "Top 5 processes by RAM usage :"
echo "$top_processes" | while read -r process
do
        process=$(echo "$process" | tr -s '[:space:]')
        p_id=$(echo "$process" | cut -d' ' -f2)
        p_mem=$(echo "$process" | cut -d' ' -f4)
        p_cmd=$(echo "$process" | cut -d' ' -f11)
        echo "  - $p_cmd (Mémoire utilisée : $p_mem) : PID $p_id"
done
echo "Listening ports :"
echo "$ports" | while read -r port
do
        port=$(echo "$port" | cut -d' ' -f4 | rev | cut -d':' -f1 | rev)
        service=$(echo "$service" | cut -d' ' -f6 | cut -d '"' -f2)
        echo "  - $port ($service)"
done
echo "Here's your random cat : $random_cat"
