#!/bin/bash

# Définition des variables
downloads_dir="/srv/yt/downloads"
logs_dir="/var/log/yt"
url=$1
date=$(date "+%D %T")

# Vérification que le dossier de téléchargement existe
if [ ! -d $downloads_dir ]
then
        echo "Le dossier de téléchargements n'existe pas."
        exit
fi

# Vérification que le dossier de logs existe
if [ ! -d $logs_dir ]
then
        echo "Le dossier de logs n'existe pas."
        exit
fi

# Vérification qu'un argument a été entré
if [ -z "$url" ]
then
        echo "Vous devez préciser un lien."
        exit
fi

# Définitions des variables
title=$(youtube-dl -e "${url}")

# Vérification que le nom de la vidéo est dispo puis téléchargement
if [ -d "/srv/yt/downloads/${title}" ]
then
        echo "Une vidéo avec le même titre a déjà été téléchargée."
        echo "Avant de télécharger, vous devez donc supprimer le dossier : ${title}"
        exit
fi
mkdir "/srv/yt/downloads/${title}"
path="/srv/yt/downloads/${title}/${title}.mp4"
youtube-dl -o "${path}" --format mp4 "$url" > /dev/null
youtube-dl --get-description "$url" > "/srv/yt/downloads/$title/description"
echo "Video ${url} was downloaded"
echo "File path : ${path}"

# Écriture dans les logs
echo "[${date}] Video ${url} was downloaded. File path : ${path}" >> /var/log/yt/download.log