# TP3 : A little script

- [TP 3 : A little script](#tp-3--a-little-script)
- [I. Script carte d'identité](#i-script-carte-didentité)
  - [Visualiser le script](./files/idcard.sh)
- [II. Script youtube-dl](#ii-script-youtube-dl)
  - [Visualiser le script (V1)](./files/yt.sh)
- [III. MAKE IT A SERVICE](#iii-make-it-a-service)
  - [Visualiser le fichier service](./files/yt.service)
  - [Visualiser le script (V2)](./files/yt-v2.sh)
- [Bonus](#iv-bonus)


## I. Script carte d'identité

Exemple d'utilisation :

```bash
luka@node1:~$ sudo sh /srv/idcard/idcard.sh
Machine name : node1.tp3.linux
OS : Ubuntu and kernel version is 5.11.0-40-generic
IP : 192.168.216.12/24
RAM : 1,0G / 2,0G
Disque : 2,7G space left
Top 5 processes by RAM usage :
        - xfwm4 (Mémoire utilisée : 4.2) : PID 908
        - /usr/lib/xorg/Xorg (Mémoire utilisée : 3.6) : PID 536
        - /usr/lib/x86_64-linux-gnu/tumbler-1/tumblerd (Mémoire utilisée : 2.9) : PID 1739
        - xfdesktop (Mémoire utilisée : 2.3) : PID 944
        - Thunar (Mémoire utilisée : 2.3) : PID 932
Listening ports :
        - 53 ()
        - 22 ()
        - 631 ()
        - 22 ()
        - 631 ()
Here's your random cat : https://cdn2.thecatapi.com/images/ae1.jpg
```

#### ➜ [**Visualiser le script**](./files/idcard.sh)

## II. Script youtube-dl

Exemple d'utilisation :

```bash
luka@node1:~$ sudo sh /srv/yt/yt.sh https://www.youtube.com/watch?v=7CFu4856xx0
Video https://www.youtube.com/watch?v=7CFu4856xx0 was downloaded
File path : /srv/yt/downloads/PORTES OUVERTES - 06 Juin 2021 | JBDS DANCESCHOOL/PORTES OUVERTES - 06 Juin 2021 | JBDS DANCESCHOOL.mp4
```

#### ➜ [**Visualiser le script**](./files/yt.sh)

## III. MAKE IT A SERVICE

- Commande `systemctl status yt`

```bash
luka@node1:~$ sudo systemctl status yt
● yt.service - Telechargement de videos YouTube
     Loaded: loaded (/etc/systemd/system/yt.service; disabled; vendor preset: enabled)
     Active: activating (auto-restart) since Mon 2021-11-22 23:38:30 CET; 6s ago
    Process: 3088 ExecStart=/usr/bin/sudo sh /srv/yt/yt-v2.sh (code=exited, status=0/SUCCESS)
   Main PID: 3088 (code=exited, status=0/SUCCESS)

nov. 22 23:38:30 node1.tp3.linux systemd[1]: yt.service: Succeeded.
```

- Extrait de `journalctl -xe -u yt`

```bash
luka@node1:~$ journalctl -xe -u yt
[...]
-- The job identifier is 7158 and the job result is done.
nov. 22 23:39:51 node1.tp3.linux systemd[1]: Started Telechargement de videos YouTube.
-- Subject: A start job for unit yt.service has finished successfully
-- Defined-By: systemd
-- Support: http://www.ubuntu.com/support
--
-- A start job for unit yt.service has finished successfully.
--
-- The job identifier is 7158.
nov. 22 23:39:51 node1.tp3.linux sudo[3178]:     root : TTY=unknown ; PWD=/ ; USER=root ; COMMAND=/usr/bin/sh /srv/yt/yt-v2.sh
nov. 22 23:39:51 node1.tp3.linux sudo[3178]: pam_unix(sudo:session): session opened for user root by (uid=0)
[...]
```

- Commande pour démarrer automatiquement le service lorsque la machine démarre

```bash
luka@node1:~$ sudo systemctl enable yt
Created symlink /etc/systemd/system/multi-user.target.wants/yt.service → /etc/systemd/system/yt.service.
```

#### ➜ [**Visualiser le fichier service**](./files/yt.service)
#### ➜ [**Visualiser le script**](./files/yt-v2.sh)

## IV. Bonus