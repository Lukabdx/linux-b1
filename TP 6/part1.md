# Partie 1 : Préparation de la machine `backup.tp6.linux`

- [Partie 1 : Préparation de la machine backup.tp6.linux](#partie-1-préparation-de-la-machine-backuptp6linux)
- [I. Ajout de disque](#i-ajout-de-disque)
- [II. Partitioning](#ii-partitioning)
- [Autres parties](#autres-parties)

## I. Ajout de disque

🌞 **Ajouter un disque dur de 5Go à la VM `backup.tp6.linux`**

```bash
# Avant d'ajouter le disque
[luka@backup ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0    8G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0    7G  0 part
  ├─rl-root 253:0    0  6.2G  0 lvm  /
  └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
sr0          11:0    1 1024M  0 rom

# On éteint et on ajoute le disque
[luka@backup ~]$ sudo shutdown -h now

# Après avoir ajouté le disque
[luka@backup ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0    8G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0    7G  0 part
  ├─rl-root 253:0    0  6.2G  0 lvm  /
  └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
sdb           8:16   0    5G  0 disk
sr0          11:0    1 1024M  0 rom

# Affichage de notre nouveau disque
[luka@backup ~]$ lsblk | grep sdb
sdb           8:16   0    5G  0 disk
```

## II. Partitioning

🌞 **Partitionner le disque à l'aide de LVM**

- Création d'un Physical Volume (PV)
    ```bash
    # Création du PV
    [luka@backup ~]$ sudo pvcreate /dev/sdb
    Physical volume "/dev/sdb" successfully created.

    # Vérifications que le PV a été crée
    [luka@backup ~]$ sudo pvs | grep sdb
    /dev/sdb      lvm2 ---   5.00g 5.00g

    [luka@backup ~]$ sudo pvdisplay
    [...]
    "/dev/sdb" is a new physical volume of "5.00 GiB"
    --- NEW Physical volume ---
    PV Name               /dev/sdb
    VG Name
    PV Size               5.00 GiB
    Allocatable           NO
    PE Size               0
    Total PE              0
    Free PE               0
    Allocated PE          0
    PV UUID               iRIaNf-B5j5-JoDn-fDB8-VJfi-Gy09-AAOdOU
    ```

- Création d'un nouveau Volume Group (VG)
    ```bash
    # Création du VG
    [luka@backup ~]$ sudo vgcreate backup /dev/sdb
    Volume group "backup" successfully created

    # Vérification que le VG a été crée
    [luka@backup ~]$ sudo vgs | grep backup
    backup   1   0   0 wz--n- <5.00g <5.00g

    [luka@backup ~]$ sudo vgdisplay
    --- Volume group ---
    VG Name               backup
    System ID
    Format                lvm2
    Metadata Areas        1
    Metadata Sequence No  1
    VG Access             read/write
    VG Status             resizable
    MAX LV                0
    Cur LV                0
    Open LV               0
    Max PV                0
    Cur PV                1
    Act PV                1
    VG Size               <5.00 GiB
    PE Size               4.00 MiB
    Total PE              1279
    Alloc PE / Size       0 / 0
    Free  PE / Size       1279 / <5.00 GiB
    VG UUID               eMblT5-Ps8o-l53E-LZhM-tqUZ-6IPM-fhPegl
    [...]
    ```

- Création d'un nouveau Logical Volume (LV)
    ```bash
    # Création du LV
    [luka@backup ~]$ sudo lvcreate -l 100%FREE backup -n backup
    Logical volume "backup" created.

    # Vérification que le LV a été crée
    [luka@backup ~]$ sudo lvs | grep backup
    backup backup -wi-a-----  <5.00g

    [luka@backup ~]$ sudo lvdisplay
    --- Logical volume ---
    LV Path                /dev/backup/backup
    LV Name                backup
    VG Name                backup
    LV UUID                R3lKQm-3nID-9AXN-TmWQ-GqQN-HeeS-Iyib2Q
    LV Write Access        read/write
    LV Creation host, time backup.tp6.linux, 2021-12-03 16:51:56 +0100
    LV Status              available
    # open                 0
    LV Size                <5.00 GiB
    Current LE             1279
    Segments               1
    Allocation             inherit
    Read ahead sectors     auto
    - currently set to     8192
    Block device           253:2
    [...]
    ```

---

🌞 **Formater la partition**

```bash
# Récupération du chemin de la partition
[luka@backup ~]$ sudo lvdisplay | grep backup | grep Path
  LV Path                /dev/backup/backup

# Formatage de la partition
[luka@backup ~]$ sudo mkfs /dev/backup/backup
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 1309696 4k blocks and 327680 inodes
Filesystem UUID: 0f7bd89e-19a4-46b1-86b7-35802f2dae81
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912, 819200, 884736

Allocating group tables: done
Writing inode tables: done
Writing superblocks and filesystem accounting information: done
```

---

🌞 **Monter la partition**

- Montage de la partition (commande `mount`)
    ```bash
    # Création du dossier "/backup"
    [luka@backup ~]$ sudo mkdir /backup

    # Montage de la partition dans ce dossier
    [luka@backup ~]$ sudo mount /dev/backup/backup /backup

    # Vérification que la partition est bien montée
    [luka@backup ~]$ df -h | grep backup
    /dev/mapper/backup-backup  5.0G   10M  4.7G   1% /backup

    # Vérification qu'il est possible de lire et écrire des données sur cette partition
    [luka@backup ~]$ ls -al / | grep backup
    drwxr-xr-x.   3 root root 4096 Dec  3 16:56 backup
    ```

- Définir un montage automatique de la partition (fichier `/etc/fstab`)
    ```bash
    # Ouverture du fichier /etc/fstab en root (pour avoir les droits de modification)
    [luka@backup ~]$ sudo nano /etc/fstab

    # On colle la ligne suivante dans le fichier
    UUID=0f7bd89e-19a4-46b1-86b7-35802f2dae81 /backup                   xfs     defaults        0 0

    # On recharge le daemon avec la commande
    [luka@backup ~]$ sudo systemctl daemon-reload
    ```

## Autres parties

- [Introduction & début du TP 6](./README.md)
- [**Partie 1 : Préparation de la machine backup.tp6.linux**](./part1.md)
- [Partie 2 : Setup du serveur NFS sur backup.tp6.linux](./part2.md)
- [Partie 3 : Setup des clients NFS : web.tp6.linux et db.tp6.linux](./part3.md)
- [Partie 4 : Scripts de sauvegarde](./part4.md)