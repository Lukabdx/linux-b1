# Partie 3 : Setup des clients NFS : `web.tp6.linux` et `db.tp6.linux`

- [Partie 3 : Setup des clients NFS : web.tp6.linux et db.tp6.linux](#partie-3-setup-des-clients-nfs-webtp6linux-et-dbtp6linux)
- [I. Sur web.tp6.linux](#i-sur-webtp6linux)
- [II. Sur web.tp6.linux](#ii-sur-dbtp6linux)
- [Autres parties](#autres-parties)

## I. Sur web.tp6.linux

🌞 **Installation**

```bash
En cours...
```

---

🌞 **Configuration**

```bash
En cours...
```

---

🌞 **Montage**

```bash
En cours...
```

## II. Sur db.tp6.linux

🌞 **Installation**

```bash
En cours...
```

---

🌞 **Configuration**

```bash
En cours...
```

---

🌞 **Montage**

```bash
En cours...
```

## Autres parties

- [Introduction & début du TP 6](./README.md)
- [Partie 1 : Préparation de la machine backup.tp6.linux](./part1.md)
- [Partie 2 : Setup du serveur NFS sur backup.tp6.linux](./part2.md)
- [**Partie 3 : Setup des clients NFS : web.tp6.linux et db.tp6.linux**](./part3.md)
- [Partie 4 : Scripts de sauvegarde](./part4.md)