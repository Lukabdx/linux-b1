# Partie 2 : Setup du serveur NFS sur `backup.tp6.linux`

- [Partie 2 : Setup du serveur NFS sur backup.tp6.linux](#partie-2-setup-du-serveur-nfs-sur-backuptp6linux)
- [Autres parties](#autres-parties)

🌞 **Préparer les dossiers à partager**

```bash
# Accès au dossier backup
[luka@backup ~]$ cd /backup

# Création des dossiers
[luka@backup backup]$ sudo mkdir web.tp6.linux
[luka@backup backup]$ sudo mkdir db.tp6.linux

# Vérification que les dossiers ont été crées
[luka@backup backup]$ ls
db.tp6.linux  web.tp6.linux
```

---

🌞 **Install du serveur NFS**

```bash
[luka@backup ~]$ sudo dnf -y install nfs-utils
[...]
Complete!
```

---

🌞 **Conf du serveur NFS**

- Fichier `/etc/idmapd.conf`
    ```bash
    # Ouverture du fichier
    [luka@backup ~]$ sudo nano /etc/idmapd.conf

    # Modification de la ligne "Domain"
    Domain = tp6.linux
    
    # Vérification que la modification a bien été enregistrée
    [luka@backup ~]$ cat /etc/idmapd.conf | grep Domain\ \=
    Domain = tp6.linux
    ```

- Fichier `/etc/exports`
    ```bash
    # Ouverture du fichier
    [luka@backup ~]$ sudo nano /etc/exports

    # Ajout des deux dossiers à partager
    /backup/web.tp6.linux 10.6.1.0/24(rw,no_root_squash)
    /backup/db.tp6.linux 10.6.1.0/24(rw,no_root_squash)

    # Vérfication que la modification a bien été enregistrée
    [luka@backup ~]$ cat /etc/exports
    /backup/web.tp6.linux 10.6.1.0/24(rw,no_root_squash)
    /backup/db.tp6.linux 10.6.1.0/24(rw,no_root_squash)
    ```

    Explication des options entre parenthèses :
    
    - rw : permet la lecture et l'écriture (par défaut, les partages sont en mode ro; c'est-à-dire en lecture seule).
    
    - no_root_squash : n'effectue pas de mapping pour l'utilisateur root.

---

🌞 **Démarrez le service**

```bash
# Démarrage du service
[luka@backup ~]$ sudo systemctl start nfs-server

# Preuve que le service est bien démarré
[luka@backup ~]$ systemctl status nfs-server
● nfs-server.service - NFS server and services
   Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; disabled; vendor preset: disabled)
   Active: active (exited) since Tue 2021-12-07 18:30:15 CET; 5s ago
  Process: 23971 ExecStart=/bin/sh -c if systemctl -q is-active gssproxy; then systemctl reload gssproxy ; fi (code=exited, status=0/SUCCESS)
  Process: 23959 ExecStart=/usr/sbin/rpc.nfsd (code=exited, status=0/SUCCESS)
  Process: 23958 ExecStartPre=/usr/sbin/exportfs -r (code=exited, status=0/SUCCESS)
 Main PID: 23971 (code=exited, status=0/SUCCESS)

Dec 07 18:30:15 backup.tp6.linux systemd[1]: Starting NFS server and services...
Dec 07 18:30:15 backup.tp6.linux systemd[1]: Started NFS server and services.

# Activation du lancement automatique du service au démarrage
[luka@backup ~]$ systemctl enable nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
```

---

🌞 **Firewall**

```bash
# Ouverture du port 2049/tcp
[luka@backup ~]$ sudo firewall-cmd --add-port=2049/tcp --permanent
success

# Preuve que la machine écoute sur le port 2049/tcp
[luka@backup ~]$ ss -alnpt | grep 2049
LISTEN 0      64           0.0.0.0:2049       0.0.0.0:*
LISTEN 0      64              [::]:2049          [::]:*
```

## Autres parties

- [Introduction & début du TP 6](./README.md)
- [Partie 1 : Préparation de la machine backup.tp6.linux](./part1.md)
- [**Partie 2 : Setup du serveur NFS sur backup.tp6.linux**](./part2.md)
- [Partie 3 : Setup des clients NFS : web.tp6.linux et db.tp6.linux](./part3.md)
- [Partie 4 : Scripts de sauvegarde](./part4.md)