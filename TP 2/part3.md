# Partie 3 : Création de votre propre service

- [Partie 3 : Création de votre propre service](#partie-3--création-de-votre-propre-service)
- [II. Jouer avec netcat](#ii-jouer-avec-netcat)
- [III. Un service basé sur netcat](#iii-un-service-basé-sur-netcat)
  - [1. Créer le service](#1-créer-le-service)
  - [2. Test test et retest](#2-test-test-et-retest)



## II. Jouer avec netcat

🌞 **Commandes pour établir un chat avec `netcat`**

- la commande tapée sur la VM
    ```bash
    luka@node1:~$ nc -l 25565
    ```
- la commande tapée sur votre PC
    ```
    C:\Users\Luka\Documents\MobaXterm\slash\bin>nc.exe 192.168.216.11 25565
    ```

🌞 **Utiliser `netcat` pour stocker les données échangées dans un fichier**

```
luka@node1:~/Desktop$ nc -l 25565 > netcat.txt
luka@node1:~/Desktop$ cat netcat.txt
Ceci est un test d'utilisation de netcat afin d'écrire dans le fichier netcat.txt depuis mon PC.
```

# III. Un service basé sur netcat

## 1. Créer le service

🌞 **Créer un nouveau service**

```bash
luka@node1:/etc/systemd/system$ sudo nano chat_tp2.service
luka@node1:/etc/systemd/system$ sudo chmod 777 chat_tp2.service
luka@node1:/etc/systemd/system$ ls -al
[...]
-rwxrwxrwx  1 root root  122 nov.   8 11:21 chat_tp2.service
[...]
luka@node1:/etc/systemd/system$ which nc
/usr/bin/nc
luka@node1:/etc/systemd/system$ cat chat_tp2.service
[Unit]
Description=Little chat service (TP2)

[Service]
ExecStart=/usr/bin/nc -l 25565

[Install]
WantedBy=multi-user.target
```

## 2. Test test et retest

🌞 **Tester le nouveau service**

```bash
# Lancement du service
luka@node1:~$ sudo systemctl start chat_tp2

# Vérification de l'état du service
luka@node1:~$ systemctl status chat_tp2
● chat_tp2.service - Little chat service (TP2)
     Loaded: loaded (/etc/systemd/system/chat_tp2.service; disabled; vendor preset: enabled)
     Active: active (running) since Mon 2021-11-08 11:33:54 CET; 11s ago
   Main PID: 1908 (nc)
      Tasks: 1 (limit: 2312)
     Memory: 192.0K
     CGroup: /system.slice/chat_tp2.service
             └─1908 /usr/bin/nc -l 25565

nov. 08 11:33:54 node1.tp2.linux systemd[1]: Started Little chat service (TP2).

# Vérification que netcat écoute bien le port 25565 (port choisi)
luka@node1:$ ss -l
tcp   LISTEN 0      1                                         0.0.0.0:25565                     0.0.0.0:*
```

Tests de fonctionnement :

- Sur le PC
```
C:\Users\Luka\Documents\MobaXterm\slash\bin>nc.exe 192.168.216.11 25565
Test d'envoi de messages depuis le PC
```
- Sur la VM
```
# Visualisation des logs avec : systemctl status
luka@node1:~$ systemctl status chat_tp2
● chat_tp2.service - Little chat service (TP2)
     Loaded: loaded (/etc/systemd/system/chat_tp2.service; disabled; vendor preset: enabled)
     Active: active (running) since Mon 2021-11-08 11:33:54 CET; 7min ago
   Main PID: 1908 (nc)
      Tasks: 1 (limit: 2312)
     Memory: 220.0K
     CGroup: /system.slice/chat_tp2.service
             └─1908 /usr/bin/nc -l 25565

nov. 08 11:33:54 node1.tp2.linux systemd[1]: Started Little chat service (TP2).
nov. 08 11:35:10 node1.tp2.linux nc[1908]: Test d'envoi de messages depuis le PC

# Visualisation des logs complets du service
luka@node1:$ journalctl -xe -u chat_tp2
-- Logs begin at Tue 2021-10-19 16:39:44 CEST, end at Mon 2021-11-08 11:40:53 CET. --
nov. 08 11:33:54 node1.tp2.linux systemd[1]: Started Little chat service (TP2).
-- Subject: A start job for unit chat_tp2.service has finished successfully
-- Defined-By: systemd
-- Support: http://www.ubuntu.com/support
--
-- A start job for unit chat_tp2.service has finished successfully.
--
-- The job identifier is 2573.
nov. 08 11:35:10 node1.tp2.linux nc[1908]: Test d'envoi de messages depuis le PC
```

# Autres parties

- [Introduction & début du TP 2](./readme.md)
- [Partie 1 : Installation et configuration d'un service SSH](./part1.md)
- [Partie 2 : Installation et configuration d'un service FTP](./part2.md)
- [**Partie 3 : Création de votre propre service**](./part3.md)
