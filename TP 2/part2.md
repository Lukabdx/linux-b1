# Partie 2 : FTP

- [Partie 2 : FTP](#partie-2--ftp)
- [I. Intro](#i-intro)
- [II. Setup du serveur FTP](#ii-setup-du-serveur-ftp)
  - [1. Installation du serveur](#1-installation-du-serveur)
  - [2. Lancement du service FTP](#2-lancement-du-service-ftp)
  - [3. Etude du service FTP](#3-etude-du-service-ftp)
  - [4. Modification de la configuration du serveur](#4-modification-de-la-configuration-du-serveur)
- [Autres parties](#autres-parties)

## II. Setup du serveur FTP

### 1. Installation du serveur

🌞 **Installer le paquet `vsftpd`**

```bash
# Installation de vsftpd
luka@node1:~$ sudo apt-get install vsftpd
[...]
0 upgraded, 1 newly installed, 0 to remove and 56 not upgraded.
[...]

# Vérification que vsftpd est installé
luka@node1:~$ vsftpd -v
vsftpd: version 3.0.3

# Vérification que le service existe
luka@node1:~$ systemctl status vsftpd
● vsftpd.service - vsftpd FTP server
     Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset: enabled)
     Active: active (running) since Sat 2021-11-06 13:05:44 CET; 25s ago
   Main PID: 3590 (vsftpd)
      Tasks: 1 (limit: 2312)
     Memory: 524.0K
     CGroup: /system.slice/vsftpd.service
             └─3590 /usr/sbin/vsftpd /etc/vsftpd.conf

nov. 06 13:05:44 node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
nov. 06 13:05:44 node1.tp2.linux systemd[1]: Started vsftpd FTP server.

# Vérification que le fichier a été crée
luka@node1:~$ ls /etc | grep vsftpd
vsftpd.conf
```

### 2. Lancement du service FTP

🌞 **Lancer le service `vsftpd`**

```bash
# Lancement de vsftpd
luka@node1:~$ sudo systemctl start vsftpd

# Vérification de l'état de vsftpd
luka@node1:~$ systemctl status vsftpd
● vsftpd.service - vsftpd FTP server
     Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset: enabled)
     Active: active (running) since Sat 2021-11-06 13:05:44 CET; 25s ago
   Main PID: 3590 (vsftpd)
      Tasks: 1 (limit: 2312)
     Memory: 524.0K
     CGroup: /system.slice/vsftpd.service
             └─3590 /usr/sbin/vsftpd /etc/vsftpd.conf

# Activation du service vsftpd
luka@node1:~$ sudo systemctl enable vsftpd
Synchronizing state of vsftpd.service with SysV service script with /lib/systemd/systemd-sysv-install.
Executing: /lib/systemd/systemd-sysv-install enable vsftpd
```

### 3. Etude du service FTP

🌞 **Analyser le service en cours de fonctionnement**

- Affichage du statut du service

```bash
luka@node1:~$ systemctl status vsftpd
● vsftpd.service - vsftpd FTP server
     Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset: enabled)
     Active: active (running) since Sat 2021-11-06 15:35:01 CET; 6min ago
   Main PID: 504 (vsftpd)
      Tasks: 1 (limit: 2312)
     Memory: 716.0K
     CGroup: /system.slice/vsftpd.service
             └─504 /usr/sbin/vsftpd /etc/vsftpd.conf

nov. 06 15:35:01 node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
nov. 06 15:35:01 node1.tp2.linux systemd[1]: Started vsftpd FTP server.
```

- Affichage du/des processus liés au service

```bash
luka@node1:~$ ps -e | grep vsftpd
    504 ?        00:00:00 vsftpd
```

- Affichage le port utilisé par le service ssh

```bash
luka@node1:~$ ss -l | grep ftp
tcp     LISTEN   0        32                                                  *:ftp                                             *:*
```

- Affichage des logs du service ftp
  - Avec une commande `journalctl` :
    ```bash
    luka@node1:~$ journalctl -xe -u vsftpd
    [...]
    -- The job identifier is 133.
    nov. 08 11:52:38 node1.tp2.linux systemd[1]: Started vsftpd FTP server.
    [...]
    ```
  - En consultant un fichier dans `/var/log` :
    ```bash
    luka@node1:~$ sudo less /var/log/vsftpd.log
    [...]
    ```

---

🌞 **Connexion au serveur ftp**

Pour autoriser les upload en FTP :

```bash
# Modification de la configuration de vsftpd pour autoriser les uploads
luka@node1:~$ sudo nano /etc/vsftpd.conf
[...]
write_enable=YES
[...]

# Restart du service pour appliquer les changements
luka@node1:~$ sudo systemctl restart vsftpd
```

Connexion et utilisation du FTP :

```bash
# Accès aux logs
luka@node1:~$ sudo less /var/log/vsftpd.log

# Ligne de log à la connexion
Mon Nov  8 19:02:43 2021 [pid 1340] CONNECT: Client "::ffff:192.168.216.1"
Mon Nov  8 19:02:43 2021 [pid 1339] [luka] OK LOGIN: Client "::ffff:192.168.216.1"

# Ligne de log à l'upload
Sat Nov  6 18:46:41 2021 [pid 1818] [luka] OK UPLOAD: Client "::ffff:192.168.216.1", "/home/luka/Desktop/salut.txt", 33 bytes, 4.42Kbyte/sec

# Ligne de log au download
Sun Nov  7 10:40:18 2021 [pid 2154] [luka] OK DOWNLOAD: Client "::ffff:192.168.216.1", "/home/luka/Desktop/salut.txt", 33 bytes, 7.82Kbyte/sec
```

🌞 **Visualiser les logs**

Commande d'accès aux logs :
```
luka@node1:~$ sudo less /var/log/vsftpd.log
```

- Ligne de log à l'upload
```
Sat Nov  6 18:46:41 2021 [pid 1818] [luka] OK UPLOAD: Client "::ffff:192.168.216.1", "/home/luka/Desktop/salut.txt", 33 bytes, 4.42Kbyte/sec
```

- Ligne de log au download
```
Sun Nov  7 10:40:18 2021 [pid 2154] [luka] OK DOWNLOAD: Client "::ffff:192.168.216.1", "/home/luka/Desktop/salut.txt", 33 bytes, 7.82Kbyte/sec
```

### 4. Modification de la configuration du serveur

🌞 **Modifier le comportement du service**

```bash
# Changement du port dans la config de vsftpd
luka@node1:~$ sudo nano /etc/vsftpd.conf
[...]
listen_port=25560
[...]

# Vérification que les modifications sont bien enregistrées
luka@node1:~$ cat /etc/vsftpd.conf | grep port
listen_port=25560
[...]

# Restart du service pour appliquer les changements
luka@node1:~$ sudo systemctl restart vsftpd
```

---

🌞 **Connexion au serveur ftp avec le nouveau port**

```bash
# Accès aux logs
luka@node1:~$ sudo less /var/log/vsftpd.log

# Ligne de log à la connexion
Mon Nov  8 23:34:34 2021 [pid 1678] CONNECT: Client "::ffff:192.168.216.1"
Mon Nov  8 23:34:34 2021 [pid 1677] [luka] OK LOGIN: Client "::ffff:192.168.216.1"

# Ligne de log à l'upload
Mon Nov  8 23:34:34 2021 [pid 1679] [luka] OK UPLOAD: Client "::ffff:192.168.216.1", "/home/luka/Desktop/salut-nouveauport.txt", 33 bytes, 4.73Kbyte/sec

# Ligne de log au download
Mon Nov  8 23:34:38 2021 [pid 1682] [luka] OK DOWNLOAD: Client "::ffff:192.168.216.1", "/home/luka/Desktop/salut-nouveauport.txt", 33 bytes, 11.05Kbyte/sec
```


# Autres parties

- [Introduction & début du TP 2](./readme.md)
- [Partie 1 : Installation et configuration d'un service SSH](./part1.md)
- [**Partie 2 : Installation et configuration d'un service FTP**](./part2.md)
- [Partie 3 : Création de votre propre service](./part3.md)