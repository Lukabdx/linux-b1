# Partie 1 : SSH

- [Partie 1 : SSH](#partie-1--ssh)
- [II. Setup du serveur SSH](#ii-setup-du-serveur-ssh)
  - [1. Installation du serveur](#1-installation-du-serveur)
  - [2. Lancement du service SSH](#2-lancement-du-service-ssh)
  - [3. Etude du service SSH](#3-etude-du-service-ssh)
  - [4. Modification de la configuration du serveur](#4-modification-de-la-configuration-du-serveur)
- [Autres parties](#autres-parties)

## II. Setup du serveur SSH

### 1. Installation du serveur

🌞 **Installer le paquet `openssh-server`**

```bash
# Installation de openssh-server
luka@node1:~$ sudo apt install openssh-server
[...]
Do you want to continue ? Y
[...]

# Vérification que openssh est installé
luka@node1:~$ ssh -V
OpenSSH_8.2p1 Ubuntu-4ubuntu0.3, OpenSSL 1.1.1f 31 Mar 2020

# Vérification que le service existe
luka@node1:~$ systemctl status ssh
● ssh.service - OpenBSD Secure Shell server
     Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2021-11-01 21:11:10 CET; 11min ago
       Docs: man:sshd(8)
             man:sshd_config(5)
    Process: 2939 ExecStartPre=/usr/sbin/sshd -t (code=exited, status=0/SUCCESS)
   Main PID: 2940 (sshd)
      Tasks: 1 (limit: 1105)
     Memory: 2.9M
     CGroup: /system.slice/ssh.service
             └─2940 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups

# Vérification que le fichier a été crée
luka@node1:~$ ls /etc | grep ssh
ssh 
```

### 2. Lancement du service SSH

🌞 **Lancer le service ssh**

```bash
# Lancement du service ssh
luka@node1:~$ sudo systemctl start ssh

# Vérification de l'état du service
luka@node1:~$ systemctl status ssh
● ssh.service - OpenBSD Secure Shell server
     Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2021-11-01 21:11:10 CET; 11min ago
       Docs: man:sshd(8)
             man:sshd_config(5)
    Process: 2939 ExecStartPre=/usr/sbin/sshd -t (code=exited, status=0/SUCCESS)
   Main PID: 2940 (sshd)
      Tasks: 1 (limit: 1105)
     Memory: 2.9M
     CGroup: /system.slice/ssh.service
             └─2940 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups

# Activation du service ssh
luka@node1:~$ sudo systemctl enable ssh
Synchronizing state of ssh.service with SysV service script with /lib/systemd/systemd-sysv-install.
Executing: /lib/systemd/systemd-sysv-install enable ssh
```

### 3. Etude du service SSH

🌞 **Analyser le service en cours de fonctionnement**

- Affichage du statut du service

```bash
luka@node1:~$ systemctl status ssh
● ssh.service - OpenBSD Secure Shell server
     Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2021-11-01 21:11:10 CET; 11min ago
       Docs: man:sshd(8)
             man:sshd_config(5)
    Process: 2939 ExecStartPre=/usr/sbin/sshd -t (code=exited, status=0/SUCCESS)
   Main PID: 2940 (sshd)
      Tasks: 1 (limit: 1105)
     Memory: 2.9M
     CGroup: /system.slice/ssh.service
             └─2940 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
```

- Affichage du/des processus liés au service

```bash
luka@node1:~$ ps -e | grep ssh
    536 ?        00:00:00 sshd
    873 ?        00:00:00 ssh-agent
```

- Affichage le port utilisé par le service ssh

```bash
luka@node1:~$ ss -l | grep ssh
u_str   LISTEN   0        4096             /run/user/1000/gnupg/S.gpg-agent.ssh 26337                                           * 0
u_str   LISTEN   0        10                         /run/user/1000/keyring/ssh 26898                                           * 0
u_str   LISTEN   0        128                   /tmp/ssh-28HWY1j2HyJx/agent.790 26623                                           * 0
tcp     LISTEN   0        128                                           0.0.0.0:ssh                                       0.0.0.0:*
tcp     LISTEN   0        128                                              [::]:ssh                                          [::]:*
```

- Affichage des logs du service ssh
  - Avec une commande `journalctl` :
    ```bash
    luka@node1:~$ journalctl -xe -u ssh
    [...]
    -- The job identifier is 100.
    nov. 08 11:54:09 node1.tp2.linux sshd[1178]: Accepted password for luka from 192.168.216.1 port 1070 ssh2
    nov. 08 11:54:09 node1.tp2.linux sshd[1178]: pam_unix(sshd:session): session opened for user luka by (uid=0)
    [...]
    ```
  - En consultant un fichier dans `/var/log` :
    ```bash
    luka@node1:~$ cat /var/log/syslog | grep ssh
    Nov  8 10:35:43 node1 systemd[705]: Listening on GnuPG cryptographic agent (ssh-agent emulation).
    Nov  8 10:39:07 node1 systemd[781]: Listening on GnuPG cryptographic agent (ssh-agent emulation).
    Nov  8 10:39:17 node1 systemd[705]: gpg-agent-ssh.socket: Succeeded.
    Nov  8 10:39:17 node1 systemd[705]: Closed GnuPG cryptographic agent (ssh-agent emulation).
    ```

---

🌞 **Connexion au serveur ssh**
```
C:\Users\Utilisateur> ssh luka@192.168.56.101
luka@192.168.56.101's password:
Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.11.0-38-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

99 updates can be applied immediately.
43 of these updates are standard security updates.
To see these additional updates run: apt list --upgradable

Your Hardware Enablement Stack (HWE) is supported until April 2025.

The programs included with the Ubuntu system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Ubuntu comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
applicable law.

luka@node1:~$
```

### 4. Modification de la configuration du serveur

🌞 **Modifier le comportement du service**

```bash
# Changement du port dans la config de SSH
luka@node1:~$ sudo nano /etc/ssh/sshd_config

# Vérification que les modification soient bien enregistrées
luka@node1:~$ cat /etc/ssh/sshd_config | grep Port
Port 1025
[...]

# Restart du service pour appliquer les changements
luka@node1:~$ sudo systemctl restart ssh
```

---

🌞 **Connexion au serveur ssh avec le nouveau port**

```
C:\Users\Luka>ssh luka@192.168.216.11 -p 1025
luka@192.168.216.11's password:
Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.11.0-38-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

56 updates can be applied immediately.
To see these additional updates run: apt list --upgradable

Your Hardware Enablement Stack (HWE) is supported until April 2025.
Last login: Sat Nov  6 12:58:53 2021 from 192.168.216.1
luka@node1:~$
```

---

# Autres parties

- [Introduction & début du TP 2](./readme.md)
- [**Partie 1 : Installation et configuration d'un service SSH**](./part1.md)
- [Partie 2 : Installation et configuration d'un service FTP](./part2.md)
- [Partie 3 : Création de votre propre service](./part3.md)