# TP2 : Explorer et manipuler le système

- [TP2 : Explorer et manipuler le système](#tp2--explorer-et-manipuler-le-système)
  - [2. Nommer la machine](#2-nommer-la-machine)
  - [3. Config réseau](#3-config-réseau)
- [Autres parties](#autres-parties)

## 2. Nommer la machine

🌞 **Changer le nom de la machine**

```bash
luka@luka-Virtualbox:~$ sudo hostname node1.tp2.linux
luka@luka-Virtualbox:~$ sudo echo "node1.tp2.linux" | sudo tee /etc/hostname
node1.tp2.linux
luka@luka-Virtualbox:~$ reboot

luka@node1:~$: cat /etc/hostname
node1.tp2.linux
```

## 3. Config réseau

🌞 **Config réseau fonctionnelle**

- **Depuis la VM**

```bash 
luka@node1:~$ ping -c 2 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=55 time=68.6 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=55 time=253 ms

--- 1.1.1.1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 68.634/160.874/253.115/92.240 ms
```

```bash
luka@node1:~$ ping -c 2 ynov.com
PING ynov.com (92.243.16.143) 56(84) bytes of data.
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=53 time=35.5 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=53 time=38.1 ms

--- ynov.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 35.471/36.781/38.091/1.310 ms
```

- **Depuis le PC**
```
C:\Users\Luka>ping 192.168.216.11

Envoi d’une requête 'Ping'  192.168.216.11 avec 32 octets de données :
Réponse de 192.168.216.11 : octets=32 temps<1ms TTL=64
Réponse de 192.168.216.11 : octets=32 temps<1ms TTL=64

Statistiques Ping pour 192.168.216.11:
    Paquets : envoyés = 2, reçus = 2, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
```

# Autres parties

- [**Introduction & début du TP 2**](./readme.md)
- [Partie 1 : Installation et configuration d'un service SSH](./part1.md)
- [Partie 2 : Installation et configuration d'un service FTP](./part2.md)
- [Partie 3 : Création de votre propre service](./part3.md)
