# TP4 : Une distribution orientée serveur

- [TP4 : Une distribution orientée serveur](#tp4--une-distribution-orientée-serveur)
- [II. Checklist](#ii-checklist)
- [III. Mettre en place un service](#iii-mettre-en-place-un-service)
  - [2. Install](#2-install)
  - [3. Analyse](#3-analyse)
  - [4. Visite du service web](#4-visite-du-service-web)
  - [5. Modif de la conf du serveur web](#5-modif-de-la-conf-du-serveur-web)

## II. Checklist

**➜ Configuration IP statique**

🌞 **Choisissez et définissez une IP à la VM**

- Contenu du fichier de conf (`/etc/sysconfig/network-scripts/ifcfg-enp0s8`) :
    ```
    TYPE=Ethernet
    BOOTPROTO=static
    NAME=enp0s8
    DEVICE=enp0s8
    ONBOOT=yes
    IPADDR=10.250.1.56
    NETMASK=255.255.255.0
    ```

- Preuve que les changements ont bien pris effet :
    ```bash
    [luka@localhost ~]$ ip a
    1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
        link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
        inet 127.0.0.1/8 scope host lo
        valid_lft forever preferred_lft forever
        inet6 ::1/128 scope host
        valid_lft forever preferred_lft forever
    2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        link/ether 08:00:27:25:6a:28 brd ff:ff:ff:ff:ff:ff
        inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
        valid_lft 86011sec preferred_lft 86011sec
        inet6 fe80::a00:27ff:fe25:6a28/64 scope link noprefixroute
        valid_lft forever preferred_lft forever
    3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
        link/ether 08:00:27:b7:ea:8a brd ff:ff:ff:ff:ff:ff
        inet 10.250.1.56/24 brd 10.250.1.255 scope global noprefixroute enp0s8
        valid_lft forever preferred_lft forever
        inet6 fe80::a00:27ff:feb7:ea8a/64 scope link
        valid_lft forever preferred_lft forever
    ```

---

**➜ Connexion SSH fonctionnelle**

🌞 **Preuves que :**

- Le service SSH est actif sur la machine
    ```bash
    [luka@localhost ~]$ systemctl status sshd
    ● sshd.service - OpenSSH server daemon
    Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
    Active: active (running) since Tue 2021-11-23 16:24:53 CET; 16min ago
        Docs: man:sshd(8)
            man:sshd_config(5)
    Main PID: 747 (sshd)
        Tasks: 1 (limit: 4934)
    Memory: 4.2M
    CGroup: /system.slice/sshd.service
            └─747 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes256-ctr,aes256-cbc,aes128-gcm@openssh.com,aes128-ctr,a>

    Nov 23 16:24:53 localhost.localdomain systemd[1]: Starting OpenSSH server daemon...
    Nov 23 16:24:53 localhost.localdomain sshd[747]: Server listening on 0.0.0.0 port 22.
    Nov 23 16:24:53 localhost.localdomain sshd[747]: Server listening on :: port 22.
    Nov 23 16:24:53 localhost.localdomain systemd[1]: Started OpenSSH server daemon.
    Nov 23 16:33:32 localhost.localdomain sshd[1413]: Accepted password for luka from 10.250.1.1 port 13628 ssh2
    Nov 23 16:33:32 localhost.localdomain sshd[1413]: pam_unix(sshd:session): session opened for user luka by (uid=0)
    ```

- Mise en place de l'échange de clés
    ```bash
    [luka@node1 ~]$ mkdir .ssh
    [luka@node1 ~]$ cd .ssh
    [luka@node1 .ssh]$ touch authorized_keys
    [luka@node1 .ssh]$ nano authorized_keys
    # Je colle ma clé publique dans le fichier
    [luka@node1 .ssh]$ chmod 600 authorized_keys
    [luka@node1 .ssh]$ cd
    [luka@node1 ~]$ chmod 700 .ssh/
    [luka@node1 ~]$ exit
    ```

- Connexion avec échange de clés
    ```
    PS C:\Users\Luka> ssh -i .\.ssh\id_ed25519 10.250.1.56
    Activate the web console with: systemctl enable --now cockpit.socket

    Last login: Wed Nov 24 20:15:34 2021 from 10.250.1.1
    [luka@node1 ~]$
    ```

---

**➜ Connexion SSH fonctionnelle**

🌞 **Preuves de :**

- Accès à internet
    ```bash
    [luka@localhost ~]$ ping 1.1.1.1
    PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
    64 bytes from 1.1.1.1: icmp_seq=1 ttl=54 time=72.2 ms
    64 bytes from 1.1.1.1: icmp_seq=2 ttl=54 time=196 ms
    --- 1.1.1.1 ping statistics ---
    2 packets transmitted, 2 received, 0% packet loss, time 1002ms
    rtt min/avg/max/mdev = 72.154/133.977/195.800/61.823 ms
    ```

- Résolution de nom
    ```bash
    [luka@localhost ~]$ ping play.battle-adventure.eu
    PING play.battle-adventure.eu (164.132.203.4) 56(84) bytes of data.
    64 bytes from ns3046463.ip-164-132-203.eu (164.132.203.4): icmp_seq=1 ttl=50 time=30.10 ms
    64 bytes from ns3046463.ip-164-132-203.eu (164.132.203.4): icmp_seq=2 ttl=50 time=34.7 ms
    --- play.battle-adventure.eu ping statistics ---
    2 packets transmitted, 2 received, 0% packet loss, time 1003ms
    rtt min/avg/max/mdev = 30.974/32.821/34.669/1.856 ms
    ```

---

**➜ Nommage de la machine**

🌞 **Définition de `node1.tp4.linux` comme nom à la machine**

- Contenu du fichier `/etc/hostname`
    ```bash
    [luka@node1 ~]$ cat /etc/hostname
    node1.tp4.linux
    ```

- Commande `hostname`
    ```bash
    [luka@node1 ~]$ hostname
    node1.tp4.linux
    ```

## III. Mettre en place un service

### 2. Install

🌞 **Installation de NGINX**

```bash
# Installation de NGINX
[luka@node1 ~]$ sudo dnf install nginx
[...]
Is this ok [y/N]: y
[...]
Is this ok [y/N]: y
[...]
Complete!

# Vérification que NGINX est installé
[luka@node1 ~]$ nginx -v
nginx version: nginx/1.14.1
```

### 3. Analyse

🌞 **Analyse du service NGINX**

- Utilisateur qui fait tourner le processus de NGINX
    ```bash
    [luka@node1 ~]$ ps -ef | grep nginx
    root       25455       1  0 16:59 ?        00:00:00 nginx: master process /usr/sbin/nginx
    nginx      25456   25455  0 16:59 ?        00:00:00 nginx: worker process

    # L'utilisateur qui fait touner le process nginx est donc "nginx"
    ```

- Port qu'écoute actuellement le serveur web
    ```bash
    [luka@node1 ~]$ ss -alnpt
    State             Recv-Q            Send-Q                       Local Address:Port                         Peer Address:Port
    [...]
    LISTEN            0                 128                                   [::]:80                                   [::]:*
    [...]

    # Le serveur web écoute actuellement sur le port 80
    ```

- Localisation de la racine web
    ```bash
    [luka@node1 ~]$ cat /etc/nginx/nginx.conf | grep root
        root         /usr/share/nginx/html;
    [...]

    # La racine web se trouve dans "/usr/share/nginx/html"
    ```

- Fichiers web accessibles en lecture par l'utilisateur qui lance le processus
    ```bash
    [luka@node1 ~]$ ls -al /usr/share/nginx/html/
    total 20
    drwxr-xr-x. 2 root root   99 Nov 23 16:56 .
    drwxr-xr-x. 4 root root   33 Nov 23 16:56 ..
    -rw-r--r--. 1 root root 3332 Jun 10 11:09 404.html
    -rw-r--r--. 1 root root 3404 Jun 10 11:09 50x.html
    -rw-r--r--. 1 root root 3429 Jun 10 11:09 index.html
    -rw-r--r--. 1 root root  368 Jun 10 11:09 nginx-logo.png
    -rw-r--r--. 1 root root 1800 Jun 10 11:09 poweredby.png

    # Les fichiers sont bien accessibles en lecture pour l'utilisateur qui exécute le process (------r--)
    ```

### 4. Visite du service web 

🌞 **Configuration du firewall pour autoriser le trafic vers le service NGINX**

```bash
[luka@node1 ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[luka@node1 ~]$ sudo firewall-cmd --reload
success
```

🌞 **Tester le bon fonctionnement du service**

```
PS C:\Users\Luka> curl -UseBasicParsing http://10.250.1.56:8000/

StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
[...]
```

### 5. Modif de la conf du serveur web

🌞 **Changer le port d'écoute**

```bash
# Changement du port dans la config de NGINX
[luka@node1 ~]$ sudo nano /etc/nginx/nginx.conf
[...]
    server {
        listen       8000 default_server;
        listen       [::]:8000 default_server;
[...]

# Restart du service
[luka@node1 ~]$ sudo systemctl restart nginx

# Vérification que le serveur web écoute sur le nouveau port
[luka@node1 ~]$ ss -alnpt | grep 8000
LISTEN 0      128          0.0.0.0:8000      0.0.0.0:*
LISTEN 0      128             [::]:8000         [::]:*

# Changement des ouvertures de ports dans le firewall
[luka@node1 ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[luka@node1 ~]$ sudo firewall-cmd --add-port=8000/tcp --permanent
success
[luka@node1 ~]$ sudo firewall-cmd --reload
success
```

- Preuve que nous pouvons visiter le port 8000
    ```
    PS C:\Users\Luka> curl -UseBasicParsing http://10.250.1.56:8000/


    StatusCode        : 200
    StatusDescription : OK
    Content           : <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
    [...]
    ```

---

🌞 **Changer l'utilisateur qui lance le service**

- Création du nouvel utilisateur `web`
    ```bash
    # Création de l'utilisateur et création d'un mot de passe
    [luka@node1 ~]$ sudo useradd web -m
    [luka@node1 ~]$ sudo passwd web
    Changing password for user web.
    New password:
    BAD PASSWORD: The password is shorter than 8 characters
    Retype new password:
    passwd: all authentication tokens updated successfully.

    # Vérification que l'utilisateur a bien été crée
    [luka@node1 ~]$ cat /etc/passwd | grep web
    [...]
    web:x:1001:1001::/home/web:/bin/bash
    ```

- Changement de l'utilisateur exécutant NGINX
    ```bash
    # Changement de l'utilisateur exécutant NGINX
    [luka@node1 ~]$ sudo nano /etc/nginx/nginx.conf
    [...]
    user web; # Ancienne ligne : "user nginx;"
    [...]

    # Vérification que les changements sont sauvegardés
    [luka@node1 ~]$ cat /etc/nginx/nginx.conf | grep user
    user web;
    [...]
    ```

- Application des changements
    ```bash
    # Application des changements
    [luka@node1 ~]$ sudo systemctl restart nginx

    # Vérification que les changements sont appliqués
    [luka@node1 ~]$ ps -ef | grep nginx
    root        1589       1  0 20:51 ?        00:00:00 nginx: master process /usr/sbin/nginx
    web         1590    1589  0 20:51 ?        00:00:00 nginx: worker process
    ```

---

🌞 **Changer l'emplacement de la racine Web**

- Création du nouvel emplacement
    ```bash
    # Création du dossier
    [luka@node1 ~]$ sudo mkdir -p /var/www/super_site_web

    # Vérification que le dossier existe
    [luka@node1 ~]$ ls /var/www
    super_site_web
    ```
- Création de notre `index.html`
    ```bash
    # Écriture d'une simple basile dans le fichier
    [luka@node1 ~]$ echo "<h1>Toto</h1>" | sudo tee /var/www/super_site_web/index.html
    <h1>Toto</h1>

    # Vérification que notre écriture a bien été faite
    [luka@node1 ~]$ cat /var/www/super_site_web/index.html
    <h1>Toto</h1>
    ```

- Changement de l'utilisateur propriétaire de la racine web
    ```bash
    # Changement du propriétaire
    [luka@node1 ~]$ sudo chown -R web /var/www/super_site_web/

    # Vérification que l'utilisateur propriétaire a bien été changé
    [luka@node1 ~]$ ls -al /var/www/
    total 4
    drwxr-xr-x.  3 root root   28 Nov 24 20:30 .
    drwxr-xr-x. 22 root root 4096 Nov 24 20:30 ..
    drwxr-xr-x.  2 web  root   24 Nov 24 20:34 super_site_web
    ```

- Changement de la configuration de NGINX
    ```bash
    # Changement de la racine dans la config de NGINX
    [luka@node1 ~]$ sudo nano /etc/nginx/nginx.conf
        root         /var/www/super_site_web; # Ancienne ligne : "    root         /usr/share/nginx/html;"

    # Vérification que les changements sont enregistrés
    [luka@node1 ~]$ cat /etc/nginx/nginx.conf | grep root
        root         /var/www/super_site_web;

    # Application des changements
    [luka@node1 ~]$ sudo systemctl restart nginx
    ```

- Preuve que le nouveau site est up
    ```
    PS C:\Users\Luka> curl -UseBasicParsing http://10.250.1.56:8000/

    StatusCode        : 200
    StatusDescription : OK
    Content           : <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
    [...]
    ```