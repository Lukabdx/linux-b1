# Travaux de Linux - B1

Ici seront déposés tous mes rendus de TP Linux de 1ère année (2021-2022).

### ➜ **Mes rendus de TP**

- [TP 1 : Are you dead yet ?](./TP%201)
- [TP 2 : Manipulation de services](./TP%202)
- [TP 3 : Scripting](./TP%203)
- [TP 4 : Une distribution orientée serveur](./TP%204)
- [TP 5 : P'tit cloud perso](./TP%205)
- [TP 6 : Stockage et sauvegarde](./TP%206)