# Partie 2 : Setup Web

- [II. Setup Web](#ii-setup-web)
  - [1. Install Apache](#1-install-apache)
    - [A. Apache](#a-apache)
    - [B. PHP](#b-php)
  - [2. Conf Apache](#2-conf-apache)
  - [3. Install NextCloud](#3-install-nextcloud)
  - [4. Test](#4-test)

## 1. Install Apache

### A. Apache

🌞 **Installer Apache sur la machine `db.tp5.linux`**

```bash
# Installation du paquet httpd
[luka@web ~]$ sudo dnf -y install httpd
[...]
Complete!

# Vérification que le service existe
[luka@web ~]$ systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor >
   Active: inactive (dead)
     Docs: man:httpd.service(8)
```

---

🌞 **Analyse du service Apache**

- Lancement du service `mariadb`
  ```bash
  [luka@web ~]$ sudo systemctl start httpd
  ```

- Activation du lancement automatique du service mariadb
  ```bash
  [luka@web ~]$ sudo systemctl enable httpd
  Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
  ```

- Vérification que le service est bien actif
  ```bash
  [luka@web ~]$ systemctl status httpd
  ● httpd.service - The Apache HTTP Server
    Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor p>
    Active: active (running) since Fri 2021-11-26 14:44:23 CET; 3min 41s ago
      Docs: man:httpd.service(8)
  Main PID: 6164 (httpd)
    Status: "Running, listening on: port 80"
      Tasks: 213 (limit: 4946)
    Memory: 32.6M
    CGroup: /system.slice/httpd.service
            ├─6164 /usr/sbin/httpd -DFOREGROUND
            ├─6165 /usr/sbin/httpd -DFOREGROUND
            ├─6166 /usr/sbin/httpd -DFOREGROUND
            ├─6167 /usr/sbin/httpd -DFOREGROUND
            └─6168 /usr/sbin/httpd -DFOREGROUND
  [...]
  ```

- Affichage du port
  ```bash
  [luka@web ~]$ ss -alnpt
  State   Recv-Q  Send-Q   Local Address:Port     Peer Address:Port  Process
  [...]
  LISTEN  0       128                  *:80                  *:*

  # Le service écoute sur le port 80
  ```

- Affichage des processus liés à `apache`
  ```bash
  [luka@web ~]$ ps -ef | grep apache
  apache      6165    6164  0 14:44 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
  apache      6166    6164  0 14:44 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
  apache      6167    6164  0 14:44 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
  apache      6168    6164  0 14:44 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND

  # Le processus apache est donc exécuté par l'utilisateur "apache"
  ```

---

🌞 **Un premier test**

- Ouverture du port `80`
  ```bash
  # Ouverture des ports
  [luka@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
  success

  # Rechargement du firewall pour appliquer les changements
  [luka@web ~]$ sudo firewall-cmd --reload
  success
  ```

- Accès à la page web
  ```
  PS C:\Users\Luka> curl -UseBasicParsing http://10.5.1.11/
  curl :

      HTTP Server Test Page powered by: Rocky Linux
  [...]
  ```

### B. PHP

🌞 **Installer PHP**

- Ajour des dépôts EPEL
  ```bash
  [luka@web ~]$ sudo dnf -y install epel-release
  [...]
  Complete!
  [luka@web ~]$ sudo dnf update
  [...]
  Dependencies resolved.
  Nothing to do.
  Complete!
  ```

- Ajout des dépôts REMI
  ```bash
  [luka@web ~]$ sudo dnf -y install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
  [...]
  Complete!
  [luka@web ~]$ sudo dnf -y module enable php:remi-7.4
  [...]
  Complete!
  ```

- Install de PHP et de toutes les libs PHP requises par NextCloud
  ```bash
  [luka@web ~]$ sudo dnf -y install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
  [...]
  Complete!
  ```

## 2. Conf Apache

🌞 **Analyser la conf Apache**

```
[luka@web ~]$ cat /etc/httpd/conf/httpd.conf | grep "\.d"
Include conf.modules.d/*.conf
```

---

🌞 **Créer un VirtualHost qui accueillera NextCloud**

```bash
[luka@web ~]$ cd /etc/httpd/conf.d
[luka@web conf.d]$ touch vhost.conf
[luka@web conf.d]$ sudo touch vhost.conf
# On colle la conf du VirtualHost
[luka@web conf.d]$ sudo systemctl restart httpd
```

---

🌞 **Configurer la racine web**

```bash
# Création de la racine web
[luka@web conf.d]$ sudo mkdir -p /var/www/nextcloud/html

# Changement de l'utilisateur propriétaire et du groupe associé au nouveau dossier
[luka@web conf.d]$ sudo chown apache:apache /var/www/nextcloud/html
```

---

🌞 **Configurer PHP**

```bash
# Vérification du fuseau horaire de la VM
[luka@web conf]$ timedatectl
                [...]
                Time zone: Europe/Paris (CET, +0100)
                [...]

# Définition du fuseau horaire
[luka@web conf]$ sudo nano /etc/opt/remi/php74/php.ini
[Date]
; Defines the default timezone used by the date functions
; http://php.net/date.timezone
;date.timezone = "Europe/Paris"
```

## 3. Install NextCloud

🌞 **Récupérer Nextcloud**

```bash
# Retour dans la racine de mon user (/home/luka)
[luka@web conf]$ cd

# Téléchargement de nextcloud
[luka@web ~]$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
[...]

# Vérification que le fichier est bien téléchargé
[luka@web ~]$ ls
nextcloud-21.0.1.zip
```

---

🌞 **Ranger la chambre**

```bash
# On décompresse le fichier zip téléchargé
[luka@web ~]$ unzip nextcloud-21.0.1.zip

# On déplace le contenu du dossier vers notre racine web
[luka@web ~]$ sudo mv nextcloud/* /var/www/nextcloud/html/
[luka@web ~]$ sudo mv nextcloud/.htaccess /var/www/nextcloud/html/
[luka@web ~]$ sudo mv nextcloud/.user.ini /var/www/nextcloud/html/

# On applique les bonnes permissions
[luka@web ~]$ sudo chown -R apache:apache /var/www/nextcloud/html

# On remove les fichiers restants
[luka@web ~]$ rm -R nextcloud
[luka@web ~]$ rm nextcloud-21.0.1.zip
```

## 4. Test

🌞 **Modifiez le fichier `hosts` de votre PC**

```bash
# Dans un terminal Windows :
C:\windows\system32\drivers\etc\hosts

# Dans notre éditeur de texte, ajouter la ligne :
10.5.1.11 web.tp5.linux

# Sauvegarder
```

---

🌞 **Tester l'accès à NextCloud et finaliser son install'**

