# Partie 1 : Setup DB

- [I. Setup DB](#partie-i--setup-db)
  - [1. Install MariaDB](#1-install-mariadb)
  - [2. Conf MariaDB](#2-conf-mariadb)
  - [3. Test](#3-test)

## 1. Install MariaDB

🌞 **Installer MariaDB sur la machine `db.tp5.linux`**

```bash
# Installation du paquet mariadb-server
[luka@db ~]$ sudo dnf -y install mariadb-server
[...]
Complete!

# Vérification que le service existe
[luka@db ~]$ systemctl status mariadb
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; disabled; vendo>
   Active: inactive (dead)
     Docs: man:mysqld(8)
           https://mariadb.com/kb/en/library/systemd/
```

---

🌞 **Le service MariaDB**

- Lancement du service `mariadb`
    ```bash
    [luka@db ~]$ sudo systemctl start mariadb
    ```

- Activation du lancement automatique du service mariadb
    ```bash
    [luka@db ~]$ sudo systemctl enable mariadb
    Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
    Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
    Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
    ```

- Vérification que le service est bien actif
    ```bash
    [luka@db ~]$ systemctl status mariadb
    ● mariadb.service - MariaDB 10.3 database server
    Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor>
    Active: active (running) since Thu 2021-11-25 17:14:00 CET; 1min 25s ago
        Docs: man:mysqld(8)
            https://mariadb.com/kb/en/library/systemd/
    Main PID: 81467 (mysqld)
    Status: "Taking your SQL requests now..."
        Tasks: 30 (limit: 4934)
    Memory: 81.7M
    CGroup: /system.slice/mariadb.service
            └─81467 /usr/libexec/mysqld --basedir=/usr
    [...]
    ```

- Affichage du port
    ```bash
    [luka@db ~]$ ss -alnpt
    State             Recv-Q            Send-Q                       Local Address:Port                         Peer Address:Port            Process
    [...]
    LISTEN            0                 80                                       *:3306                                    *:*
    [...]

    # Le service écoute sur le port 3306
    ```

- Affichage des processus liés à `mariadb` (`mysql`)
    ```bash
    [luka@db ~]$ ps -ef | grep mysql
    mysql      81467       1  0 17:14 ?        00:00:00 /usr/libexec/mysqld --basedir=/usr

    # Le processus mysql est donc exécuté par l'utilisateur "mysql"
    ```

---

🌞 **Firewall**

```bash
# Ouverture des ports
[luka@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success

# Rechargement du firewall pour appliquer les changements
[luka@web ~]$ sudo firewall-cmd --reload
success
```

## 2. Conf MariaDB

🌞 **Configuration élémentaire de la base**

```bash
[luka@db ~]$ mysql_secure_installation

[...]
# Ici, il faut entrer le mot de passe de l'utilisateur root (de la DB) si déjà défini (sinon, on laisse vide)
Enter current password for root (enter for none):
OK, successfully used password, moving on...

[...]
# Ici, le programme nous invite à créer un mot de passe pour root (BIEN EVIDEMMENT QUE OUI !!)
Set root password? [Y/n]
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!

[...]
# Ici, le programme nous invite à supprimer les "utilisateurs anonymes" crées par défaut par MariaDB (tout le monde peut se connecter avec ces utilisateurs anonymes, donc OUI BIEN SÛR.)
Remove anonymous users? [Y/n]
 ... Success!

[...]
# Ici, MariaDB nous propose de désactiver la connexion à la DB en tant que root à distance (lorsque l'ip n'est pas locale) : donc OUI.
Disallow root login remotely? [Y/n]
 ... Success!

[...]
# Ici, on nous propose de supprimer la DB de test, ses utilisateurs et le privilèges associés à cela. Comme on en veut pas : OUI
Remove test database and access to it? [Y/n]
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

[...]
# On nous propose de recharger la tables des privilèges afin de bien appliquer les changements (spoiler : OUI)
Reload privilege tables now? [Y/n]
 ... Success!

[...]
# Installation terminée et la DB est sécurisée !
Thanks for using MariaDB!
```

---

🌞 **Préparation de la base en vue de l'utilisation par NextCloud**

```bash
# Accès à mysql
[luka@db ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
[...]

# Création de l'utilisateur "nextcloud" (avec pour mot de passe "meow")
MariaDB [(none)]> CREATE USER 'nextcloud'@'10.5.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.001 sec)

# Création de la base de données "nextcloud" encodée en UTF8 si elle n'existe pas déjà
MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

# On donne tous les privilèges de la DB "nextcloud" à l'utilisateur "nextcloud"
MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.5.1.11';
Query OK, 0 rows affected (0.000 sec)

# On recharge les privilèges pour appliquer les changements
MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```

## 3. Test

🌞 **Installez sur la machine `web.tp5.linux` la commande `mysql`**

```bash
# Recherche du paquet dans lequel se trouve la commande mysql
[luka@web ~]$ dnf provides mysql
Rocky Linux 8 - AppStream                   3.2 MB/s | 8.2 MB     00:02
Rocky Linux 8 - BaseOS                      3.1 MB/s | 3.5 MB     00:01
Rocky Linux 8 - Extras                       21 kB/s |  10 kB     00:00
Last metadata expiration check: 0:00:01 ago on Fri 26 Nov 2021 02:28:12 PM CET.
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs
                                                  : and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7

# Installation du paquet
[luka@web ~]$ sudo dnf -y install mysql
[...]
Complete!

# Vérification que mysql est bien installé
[luka@web ~]$ mysql --version
mysql  Ver 8.0.26 for Linux on x86_64 (Source distribution)
```

---


🌞 **Tester la connexion**

```bash
# Connexion à la base de données
[luka@web ~]$ mysql -u nextcloud -p -h 10.5.1.12 -P 3306 -D nextcloud
Enter password:

Welcome to the MySQL monitor.  Commands end with ; or \g.
[...]

# Affichage des tables
mysql> SHOW TABLES;
Empty set (0.00 sec)

# La connexion est fonctionnelle !
```

---

C'est bon ! Ca tourne ! [**Go installer NextCloud maintenant !**](./web.md)