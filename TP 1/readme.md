# TP 1 : Are you dead yet ?

---

**🌞 Plusieurs façons différentes de péter la machine**

### ➜ Première solution :

Supprimer tous les fichiers/dossiers depuis la racine, de manière forcée :

```sudo rm -R -f /*``` -> Suppression de tous les fichiers de manière récursive (-R) et forcée (-f) en root

---

### ➜ Deuxième solution :

Création d'une tâche répétée qui va éteindre la machine toutes les minutes :

```sudo crontab -e``` -> Ouverture du gestionnaire des tâches répétées (le fichier crontab) en root

```* * * * * /sbin/shutdown -h now``` -> Ajout d'une ligne au fichier crontab signifiant que l'on veut éteindre notre machine toutes les minutes

---

### ➜ Troisième solution :

Retirer tous les droits sur tous les fichiers depuis la racine :

```sudo chmod 000 /*``` -> Mise des permissions à 000 (pas d'écriture, pas de lecture, pas d'exécution) pour tous les utilisateurs (même root)

---

### ➜ Quatième solution :

Arrêter tous les processus de root en cours (dont le système : Processus ID 1) de manière forcée :

```sudo killall -u root``` -> Arrêt forcé des processus en cours sur l'utilisateur root (killall)

---

### ➜ Cinquième solution :

Supprimer les données des utilisateurs dans /etc/passwd :

```sudo nano /etc/passwd``` -> Ouverture du fichier passwd (contenant les données des utilisateurs : nom, directory, ...) Dans ce fichier, il faudra supprimer l'ensemble des lignes puis sauvegarder.

A première vue, la machine fonctionne bien, elle reste utilisable... Jusqu'au prochain redémarrage 😈

---

### ➜ Solution ultime :

![Destruction ultime](https://media2.giphy.com/media/1Be4g2yeiJ1QfqaKvz/giphy.gif?cid=ecf05e47bpjrec1vatcqlm608xqg04y7vop7wdfq72lyzhaf&rid=giphy.gif)
